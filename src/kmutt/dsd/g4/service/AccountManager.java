package kmutt.dsd.g4.service;

import kmutt.dsd.g4.model.Account;
import kmutt.dsd.g4.util.Constants;
import kmutt.dsd.g4.util.DatabaseSource;
import kmutt.dsd.g4.util.Utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class AccountManager {

    private static String SelectUserIdSQL = "select * from account where user_id = ?";
    private static String UpdateStatusSQL = "update account set status = ? where user_id = ?";
    private static String UpdateChangePassSQL = "update account set password = ? where user_id = ?";
    private static String InsertUserSQL = "insert into account (user_id, password, display_name, email, no_lose, no_win, no_draw, status) values (?, ?, ?, ?, ?, ?, ?, ?)";
    private static String DeleteUserIdSQL = "delete from account where user_id = ?";

    public static Account login(String username, String pass) {
        Account account = getAccount(username);
        if (account != null && pass.equals(account.getPassword())) {
            account = updateStatus(account.getUserId(), Constants.ACCOUNT_STATUS.ONLINE);
        } else {
            account = null;
        }
        return account;
    }

    public static Account logout(String username) {
        Account account = updateStatus(username, Constants.ACCOUNT_STATUS.OFFLINE);
        return account;
    }

    public static Account register(String username, String password, String email, String displayName) {
        Account account = createNewAccount(username, password, email, displayName);
        return account;
    }

    public static Account changePassword(String username, String oldPassword, String newPassword, String newPasswordConfirm) {
        Account account = getAccount(username);
        if (account != null && !oldPassword.equals(account.getPassword())) {
            return account;
        } else if (account != null && !newPassword.equals(newPasswordConfirm)) {
            return account;
        } else {
            try {
                Connection connection = DatabaseSource.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UpdateChangePassSQL);
                preparedStatement.setString(1, newPassword);
                preparedStatement.setString(2, username);
                Integer i = preparedStatement.executeUpdate();
                if (i > 0) {
                    account = getAccount(username);
                    return account;
                }
                preparedStatement.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                return account;
            }
        }
    }

    public static Boolean removeAccount(String username) {
        Boolean result = deleteAccount(username);
        return result;
    }

    private static Account createNewAccount(String username, String password, String email, String displayName) {
        Account account = null;
        try {
            Connection connection = DatabaseSource.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(InsertUserSQL);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, displayName);
            preparedStatement.setString(4, email);
            preparedStatement.setInt(5, 0);
            preparedStatement.setInt(6, 0);
            preparedStatement.setInt(7, 0);
            preparedStatement.setString(8, Constants.ACCOUNT_STATUS.OFFLINE);
            Integer i = preparedStatement.executeUpdate();
            if (i > 0) {
                account = getAccount(username);
            }
            preparedStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return account;
        }
    }

    private static Account updateStatus(String username, String status) {
        Account account = null;
        try {
            Connection connection = DatabaseSource.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UpdateStatusSQL);
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, username);
            Integer i = preparedStatement.executeUpdate();
            if (i > 0) {
                account = getAccount(username);
            }
            preparedStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return account;
        }
    }

    private static Account getAccount(String username) {
        Account account = null;
        try {
            Connection connection = DatabaseSource.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SelectUserIdSQL);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                account = Utility.mappingAccount(resultSet, account);
            }
            preparedStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return account;
        }
    }

    private static Boolean deleteAccount(String username) {
        Boolean result = false;
        try {
            Connection connection = DatabaseSource.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DeleteUserIdSQL);
            preparedStatement.setString(1, username);
            Integer i = preparedStatement.executeUpdate();
            if (i > 0) {
                result = true;
            }
            preparedStatement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return result;
        }
    }
}

