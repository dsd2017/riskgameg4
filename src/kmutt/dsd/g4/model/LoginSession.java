package kmutt.dsd.g4.model;

import java.sql.Timestamp;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class LoginSession {
    private String userId;
    private String ipAddress;
    private Timestamp timeLogin;

    public LoginSession() {
    }

    public LoginSession(String userId, String ipAddress, Timestamp timeLogin) {
        this.userId = userId;
        this.ipAddress = ipAddress;
        this.timeLogin = timeLogin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Timestamp getTimeLogin() {
        return timeLogin;
    }

    public void setTimeLogin(Timestamp timeLogin) {
        this.timeLogin = timeLogin;
    }

    @Override
    public String toString() {
        return "LoginSession{" +
                "userId='" + userId + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", timeLogin=" + timeLogin +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginSession)) return false;

        LoginSession that = (LoginSession) o;

        if (getUserId() != null ? !getUserId().equals(that.getUserId()) : that.getUserId() != null) return false;
        if (getIpAddress() != null ? !getIpAddress().equals(that.getIpAddress()) : that.getIpAddress() != null)
            return false;
        return getTimeLogin() != null ? getTimeLogin().equals(that.getTimeLogin()) : that.getTimeLogin() == null;
    }

    @Override
    public int hashCode() {
        int result = getUserId() != null ? getUserId().hashCode() : 0;
        result = 31 * result + (getIpAddress() != null ? getIpAddress().hashCode() : 0);
        result = 31 * result + (getTimeLogin() != null ? getTimeLogin().hashCode() : 0);
        return result;
    }
}
