package kmutt.dsd.g4.model;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class GameStage {

    private Integer gameId;
    private Integer mapId;
    private String  playerId1;
    private String  playerId2;
    private String  playerId3;
    private String  playerId4;
    private String  playerId5;

    public GameStage() {
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getMapId() {
        return mapId;
    }

    public void setMapId(Integer mapId) {
        this.mapId = mapId;
    }

    public String getPlayerId1() {
        return playerId1;
    }

    public void setPlayerId1(String playerId1) {
        this.playerId1 = playerId1;
    }

    public String getPlayerId2() {
        return playerId2;
    }

    public void setPlayerId2(String playerId2) {
        this.playerId2 = playerId2;
    }

    public String getPlayerId3() {
        return playerId3;
    }

    public void setPlayerId3(String playerId3) {
        this.playerId3 = playerId3;
    }

    public String getPlayerId4() {
        return playerId4;
    }

    public void setPlayerId4(String playerId4) {
        this.playerId4 = playerId4;
    }

    public String getPlayerId5() {
        return playerId5;
    }

    public void setPlayerId5(String playerId5) {
        this.playerId5 = playerId5;
    }

    @Override
    public String toString() {
        return "GameStage{" +
                "gameId=" + gameId +
                ", mapId=" + mapId +
                ", playerId1='" + playerId1 + '\'' +
                ", playerId2='" + playerId2 + '\'' +
                ", playerId3='" + playerId3 + '\'' +
                ", playerId4='" + playerId4 + '\'' +
                ", playerId5='" + playerId5 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameStage)) return false;

        GameStage gameStage = (GameStage) o;

        if (getGameId() != null ? !getGameId().equals(gameStage.getGameId()) : gameStage.getGameId() != null)
            return false;
        if (getMapId() != null ? !getMapId().equals(gameStage.getMapId()) : gameStage.getMapId() != null) return false;
        if (getPlayerId1() != null ? !getPlayerId1().equals(gameStage.getPlayerId1()) : gameStage.getPlayerId1() != null)
            return false;
        if (getPlayerId2() != null ? !getPlayerId2().equals(gameStage.getPlayerId2()) : gameStage.getPlayerId2() != null)
            return false;
        if (getPlayerId3() != null ? !getPlayerId3().equals(gameStage.getPlayerId3()) : gameStage.getPlayerId3() != null)
            return false;
        if (getPlayerId4() != null ? !getPlayerId4().equals(gameStage.getPlayerId4()) : gameStage.getPlayerId4() != null)
            return false;
        return getPlayerId5() != null ? getPlayerId5().equals(gameStage.getPlayerId5()) : gameStage.getPlayerId5() == null;
    }

    @Override
    public int hashCode() {
        int result = getGameId() != null ? getGameId().hashCode() : 0;
        result = 31 * result + (getMapId() != null ? getMapId().hashCode() : 0);
        result = 31 * result + (getPlayerId1() != null ? getPlayerId1().hashCode() : 0);
        result = 31 * result + (getPlayerId2() != null ? getPlayerId2().hashCode() : 0);
        result = 31 * result + (getPlayerId3() != null ? getPlayerId3().hashCode() : 0);
        result = 31 * result + (getPlayerId4() != null ? getPlayerId4().hashCode() : 0);
        result = 31 * result + (getPlayerId5() != null ? getPlayerId5().hashCode() : 0);
        return result;
    }
}
