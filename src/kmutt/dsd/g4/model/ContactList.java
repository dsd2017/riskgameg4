package kmutt.dsd.g4.model;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class ContactList {
    private String userId;
    private String contactId;
    private String contactName;

    public ContactList() {
    }

    public ContactList(String userId, String contactId, String contactName) {
        this.userId = userId;
        this.contactId = contactId;
        this.contactName = contactName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Override
    public String toString() {
        return "ContactList{" +
                "userId='" + userId + '\'' +
                ", contactId='" + contactId + '\'' +
                ", contactName='" + contactName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactList)) return false;

        ContactList that = (ContactList) o;

        if (getUserId() != null ? !getUserId().equals(that.getUserId()) : that.getUserId() != null) return false;
        if (getContactId() != null ? !getContactId().equals(that.getContactId()) : that.getContactId() != null)
            return false;
        return getContactName() != null ? getContactName().equals(that.getContactName()) : that.getContactName() == null;
    }

    @Override
    public int hashCode() {
        int result = getUserId() != null ? getUserId().hashCode() : 0;
        result = 31 * result + (getContactId() != null ? getContactId().hashCode() : 0);
        result = 31 * result + (getContactName() != null ? getContactName().hashCode() : 0);
        return result;
    }
}
