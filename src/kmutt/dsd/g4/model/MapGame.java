package kmutt.dsd.g4.model;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class MapGame {
    private Integer mapId;

    public MapGame() {
    }

    public MapGame(Integer mapId) {
        this.mapId = mapId;
    }

    public Integer getMapId() {
        return mapId;
    }

    public void setMapId(Integer mapId) {
        this.mapId = mapId;
    }

    @Override
    public String toString() {
        return "MapGame{" +
                "mapId=" + mapId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MapGame)) return false;

        MapGame mapGame = (MapGame) o;

        return getMapId() != null ? getMapId().equals(mapGame.getMapId()) : mapGame.getMapId() == null;
    }

    @Override
    public int hashCode() {
        return getMapId() != null ? getMapId().hashCode() : 0;
    }
}
