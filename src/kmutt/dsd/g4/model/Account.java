package kmutt.dsd.g4.model;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class Account {
    private String userId;
    private String password;
    private String displayName;
    private String email;
    private Integer noLose;
    private Integer noWin;
    private Integer noDraw;
    private String status;

    public Account() {
    }

    public Account(String userId, String password, String displayName, String email, Integer noLose, Integer noWin, Integer noDraw) {
        this.userId = userId;
        this.password = password;
        this.displayName = displayName;
        this.email = email;
        this.noLose = noLose;
        this.noWin = noWin;
        this.noDraw = noDraw;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNoLose() {
        return noLose;
    }

    public void setNoLose(Integer noLose) {
        this.noLose = noLose;
    }

    public Integer getNoWin() {
        return noWin;
    }

    public void setNoWin(Integer noWin) {
        this.noWin = noWin;
    }

    public Integer getNoDraw() {
        return noDraw;
    }

    public void setNoDraw(Integer noDraw) {
        this.noDraw = noDraw;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Account{" +
                "userId='" + userId + '\'' +
                ", password='" + password + '\'' +
                ", displayName='" + displayName + '\'' +
                ", email='" + email + '\'' +
                ", noLose=" + noLose +
                ", noWin=" + noWin +
                ", noDraw=" + noDraw +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;

        Account account = (Account) o;

        if (getUserId() != null ? !getUserId().equals(account.getUserId()) : account.getUserId() != null) return false;
        if (getPassword() != null ? !getPassword().equals(account.getPassword()) : account.getPassword() != null)
            return false;
        if (getDisplayName() != null ? !getDisplayName().equals(account.getDisplayName()) : account.getDisplayName() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(account.getEmail()) : account.getEmail() != null) return false;
        if (getNoLose() != null ? !getNoLose().equals(account.getNoLose()) : account.getNoLose() != null) return false;
        if (getNoWin() != null ? !getNoWin().equals(account.getNoWin()) : account.getNoWin() != null) return false;
        if (getNoDraw() != null ? !getNoDraw().equals(account.getNoDraw()) : account.getNoDraw() != null) return false;
        return getStatus() != null ? getStatus().equals(account.getStatus()) : account.getStatus() == null;
    }

    @Override
    public int hashCode() {
        int result = getUserId() != null ? getUserId().hashCode() : 0;
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getDisplayName() != null ? getDisplayName().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getNoLose() != null ? getNoLose().hashCode() : 0);
        result = 31 * result + (getNoWin() != null ? getNoWin().hashCode() : 0);
        result = 31 * result + (getNoDraw() != null ? getNoDraw().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        return result;
    }
}
