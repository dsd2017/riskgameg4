package kmutt.dsd.g4;

import kmutt.dsd.g4.model.Account;
import kmutt.dsd.g4.service.AccountManager;

public class Test {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        Account accountRegister = AccountManager.register("iceicy", "1234", "icenotice@gmail.com", "iceicy" );
        if (accountRegister != null) {
            System.out.println(accountRegister.toString());
        } else {
            System.out.println("Register something wrong!");
        }

        Account accountLogin = AccountManager.login("iceicy","1234");
        if (accountLogin != null) {
            System.out.println(accountLogin.toString());
        } else {
            System.out.println("Login something wrong!");
        }

        Account accountLogout = AccountManager.logout("iceicy");
        if (accountLogout != null) {
            System.out.println(accountLogout.toString());
        } else {
            System.out.println("Logout something wrong!");
        }

        Boolean isDelete = AccountManager.removeAccount("iceicy");
        if (isDelete) {
            System.out.println("Removed!!");
        } else {
            System.out.println("Remove account something wrong!");
        }
    }
}
