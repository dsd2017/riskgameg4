package kmutt.dsd.g4.util;

import kmutt.dsd.g4.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by cupcodemini on 5/26/2017 AD.
 */
public class Utility {

    public static Account mappingAccount(ResultSet resultSet, Account account) throws SQLException {
        if (account == null)
            account = new Account();
        account.setUserId(resultSet.getString("user_id"));
        account.setPassword(resultSet.getString("password"));
        account.setDisplayName(resultSet.getString("display_name"));
        account.setEmail(resultSet.getString("email"));
        account.setNoLose(resultSet.getInt("no_lose"));
        account.setNoWin(resultSet.getInt("no_win"));
        account.setNoDraw(resultSet.getInt("no_draw"));
        account.setStatus(resultSet.getString("status"));
        return account;
    }
}
