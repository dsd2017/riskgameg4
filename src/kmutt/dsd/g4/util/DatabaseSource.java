package kmutt.dsd.g4.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by cupcodemini on 5/25/2017 AD.
 */
public class DatabaseSource {
    static final String DRIVER = "com.mysql.jdbc.Driver";
    static final String URL = "jdbc:mysql://localhost:49162/dsd2017";
    static final String USERNAME = "root";
    static final String PASSWORD = "mysql";

    private Connection connection;
    private static DatabaseSource databaseSource;

    public DatabaseSource() throws Exception{
        Class.forName(DRIVER);
        connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
    }

    public static DatabaseSource getInstance() throws Exception{
        if(databaseSource == null)
            databaseSource = new DatabaseSource();
        return databaseSource;
    }

    public Connection getConnection() {
        return connection;
    }

}