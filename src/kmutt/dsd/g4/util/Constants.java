package kmutt.dsd.g4.util;

/**
 * Created by cupcodemini on 5/18/2017 AD.
 */
public class Constants {

    public static class ACCOUNT_STATUS {
        public static final String ONLINE = "ONLINE";
        public static final String OFFLINE = "OFFLINE";
        public static final String PLAYING = "PLAYING";
    }
}
